devfest-gdriveapp
=================

Demo web app for DevFest CDO 2014 Codelab for Google Drive and Python

Before running the code from this repo, make sure to generate your JSON credentials at https://console.developers.google.com.

If you don't have access to the Google Drive API yet, make sure to activate the Drive API and the Drive SDK as well. For these steps, you can refer to the DevFest 2014 Slide Deck at https://github.com/hardwyrd/devfest2014-slidedeck.git

 
