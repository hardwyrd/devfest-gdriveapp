import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from flask import Flask, request, render_template, url_for, send_from_directory

gauth = GoogleAuth()
drive = GoogleDrive(gauth)

try:
    file_list = drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
except:
    file_list = None
    pass

app = Flask(__name__)


@app.route('/')
def hello_world():
    if file_list:
        return render_template('index.html', files=file_list)
    else:
        return render_template('fail.html')


if __name__ == '__main__':
    app.run(debug=True, port=3000)
